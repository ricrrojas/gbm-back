const express = require('express');

const router = express.Router();
require('../config/passport');
router.use('/api', require('./api'));

module.exports = router;
