const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../auth');

const Users = mongoose.model('Users');

router.post('/register', auth.optional, function (req, res, next) {
  const {
    body: { user },
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  const finalUser = new Users(user);

  finalUser.setPassword(user.password);

  return finalUser
    .save()
    .then(() => res.json({ user: finalUser.toAuthJSON() }))
    .catch((err) => {
      if (err.name === 'MongoError' && err.code === 11000) {
        return res.status(400).send({ succes: false, message: 'El usuario ya existe' });
      }
      return res.status(500).send(err);
    });
});

router.post('/login', auth.optional, function (req, res, next) {
  const {
    body: { user },
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
    if (err) {
      return next(err);
    }

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      return res.json({ user: user.toAuthJSON() });
    }

    return res.status(400).json(info);
  })(req, res, next);
});

router.get('/current', auth.required, function (req, res, next) {
  const {
    payload: { id },
  } = req;

  return Users.findById(id).then((user) => {
    if (!user) {
      return res.sendStatus(400);
    }

    return res.json({ user: user.toAuthJSON() });
  });
});

router.post('/auth/google', auth.optional, (req, res, next) => passport.authenticate('google-token', { session: false }, function (err, passportUser, info) {
  if (err) {
    return next(err);
  }

  if (passportUser) {
    const user = passportUser;
    user.token = passportUser.generateJWT();

    return res.json({ user: user.toAuthJSON() });
  }

  return res.status(400).json(info);
})(req, res, next));

module.exports = router;
