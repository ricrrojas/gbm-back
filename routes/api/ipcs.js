const router = require('express').Router();
const axios = require('axios');
const auth = require('../auth');

router.get('/', auth.required, (req, res) => axios
  .get('https://www.gbm.com.mx/Mercados/ObtenerDatosGrafico?empresa=IPC')
  .then(response => res.json(response.data))
  .catch(() => res.status(500).json({
    errors: {
      api: 'error al consumir la api',
    },
  })));

module.exports = router;
