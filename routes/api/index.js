const express = require('express');

const router = express.Router();

router.use('/users', require('./users'));
router.use('/ipcs', require('./ipcs'));

module.exports = router;
