const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const GoogleTokenStrategy = require('passport-google-token').Strategy;
const config = require('./config');

const Users = mongoose.model('Users');

passport.use(
  new LocalStrategy(
    {
      usernameField: 'user[email]',
      passwordField: 'user[password]',
    },
    (email, password, done) => {
      Users.findOne({ email })
        .then((user) => {
          if (!user || !user.validatePassword(password)) {
            return done(null, false, {
              errors: { 'email or password': 'is invalid' },
            });
          }

          return done(null, user);
        })
        .catch(done);
    },
  ),
);

passport.use(
  new GoogleTokenStrategy(
    {
      clientID: config.googleAuth.clientID,
      clientSecret: config.googleAuth.clientSecret,
    },
    (accessToken, refreshToken, profile, done) => {
      Users.upsertGoogleUser(accessToken, refreshToken, profile, (err, user) => done(err, user));
    },
  ),
);
