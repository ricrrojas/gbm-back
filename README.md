# Prueba GBM
Prueba técnica de api con autenticación user/password y google provider 
### Requisitos
* mongo 
### Instalación
    $ npm install 
## Ejecución
    $  npm run watch 
## Construido con
* [Passport.js]
* [Mongoose]
* [Express] 
## Autor
* Ricardo Rojas 