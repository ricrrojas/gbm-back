const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const UsersSchema = new Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  hash: String,
  salt: String,
  googleProvider: {
    type: {
      id: String,
      token: String,
    },
    select: false,
  },
  loginAttempts: { type: Number, required: true, default: 0 },
  lockUntil: { type: Number },
});

UsersSchema.methods.setPassword = function setPassword(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UsersSchema.methods.validatePassword = function validatePassword(password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  const validPassword = this.hash === hash;
  return validPassword;
};

UsersSchema.methods.generateJWT = function generateJWT() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign(
    {
      email: this.email,
      id: this._id,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
    },
    'secret',
  );
};

UsersSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT(),
  };
};

UsersSchema.statics.upsertGoogleUser = function upsertGoogleUser(
  accessToken,
  refreshToken,
  profile,
  cb,
) {
  const that = this;
  return this.findOne(
    {
      'googleProvider.id': profile.id,
    },
    (err, user) => {
      if (!user) {
        const newUser = new that({
          fullName: profile.displayName,
          email: profile.emails[0].value,
          googleProvider: {
            id: profile.id,
            token: accessToken,
          },
        });

        newUser.save((error, savedUser) => {
          if (error) {
            console.log(error);
          }
          return cb(error, savedUser);
        });
      } else {
        return cb(err, user);
      }
    },
  );
};

mongoose.model('Users', UsersSchema);
